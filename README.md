[![JavaScript Style Guide: Gal Bar](https://img.shields.io/badge/Gal%20Bar-linkedin-brightgreen.svg?style=flat)](https://il.linkedin.com/in/gal-bar-576638173?trk=people-guest_profile-result-card_result-card_full-click)


# Homework-assignment-1

Python test file to check if a certain string contains in a webpage, 
Tests are divided to 2 :

## test_right_click_in_html
```
check if "Right-click in the box below to see one called 'the-internet'" is in webpage
```

## test_right_click_in_html():
```
check if 'Alibaba' is in webpage
```

